# R-script for a multi-metric calibration using performance metrics and signature metrics

# third script.

# all model outputs (watout.dat) from the second script have to be saved in a folder called getwd()//results
# further input is a time series of observed discharge data for the same station and the same modeling period

# the output is a table with the calculated performance measures

# This script is based on the study of

# Pfannerstill, M.; Guse, B.; Fohrer, N. (2014):
# Smart low flow signature metrics for an improved overall performance evaluation of hydrological models, 
# J. Hydrol, 510, 447-458, doi: 10.1016/j.jhydrol.2013.12.044.

# and a further development in
# Haas, M.; Guse, B.; Pfannerstill, M.; Fohrer, N. (2016): 
# A joined multi-metric calibration of river discharge and nitrate loads with different performance measures, 
# J. Hydrol., 536, 534-545, doi:10.1016/j.jhydrol.2016.03.001

# For questions:
# Dr. Bj?rn Guse bguse@hydrology.uni-kiel.de
# Dr. Matthias Pfannerstill mpfannerstill@hydrology.uni-kiel.de

# R-packages
library(hydroGOF)
library(xts)
library(zoo)
library(hydroTSM)

#set working directory
#swat_wd <- "d:\\SWATmodel\\Treene\\"
#swat_wd <- "d:\\Lehre\\Modul 192 - Spezielle Hydrologie\\Exercise_2018\\model\\"
swat_wd <- "C:\\KlimaW\\Lippe\\Simulations\\Results"
# Results_R

setwd(swat_wd)

### calculation of flow duration curve and its segments for the observed discharge data

# table with discharge data
# first column: date
# second column: discharge value
site_discharge <- read.table(paste("C:\\KlimaW\\Lippe\\SWAT input Lippe\\Gauges\\Schermbeck 1_R.txt", sep=""), header=FALSE)
Observed_runoff_path<- 'C:\\KlimaW\\Lippe\\SWAT input Lippe\\Gauges\\Schermbeck 1_R.txt'  # Name and path of the file with the observed data
starting_date_obs<-"2010-01-01"   
### Read the observed data ###   
site_discharge <-read.table(Observed_runoff_path)

#discharge data
dates <- site_discharge[,2]
site_discharge$ID<-c(1:nrow(site_discharge))
obsdata<-site_discharge
# please consider that the date information (here "%d.%m.%Y") has to be adapted to the available discharge data
xts.obs<-as.xts(obsdata[,3], 
                order.by =as.Date(as.character(obsdata[,2]), "%d.%m.%Y"))

# calibration period  ####

# selection of the modeling period
calobsdischarge<-xts.obs['2011-11-01/2016-10-31']
# three years warm up
# five years calibration
# five years validation

# discharge data in the order of their magnitude
sort.obs <- sort(as.numeric(calobsdischarge))
sort.length<-1:length(sort.obs)

# separation of the ordered discharge data into five segments
# very high flows
obs005<- as.numeric(sort.obs[round(length(sort.obs)*0.95,0):round(length(sort.obs)*1.0,0)])
# high flows
obs0520<- as.numeric(sort.obs[round(length(sort.obs)*0.80,0):round(length(sort.obs)*0.95,0)])
obs2070<- as.numeric(sort.obs[round(length(sort.obs)*0.30,0):round(length(sort.obs)*0.80,0)])
obs7095<- as.numeric(sort.obs[round(length(sort.obs)*0.05,0):round(length(sort.obs)*0.30,0)])
# very low flows
obs95100<- as.numeric(sort.obs[1:round(length(sort.obs)*0.05,0)]) 

###

# selected performance measures
gofvector <-c("run", "NSE", "PBIAS","KGE", "RSR", 
             "KGE_alpha", "KGE_r", "KGE_beta",
             "very_high", "high", "mid", "low", "very_low")

qmod <- read.table("C:\\KlimaW\\Lippe\\Simulations\\Results\\swatplus_sim_230203.txt",
                   sep=";", header=T)

# number of simulations
nrun <- ncol(qmod)-1

# matrix to save results of performance criteria
MGOF<-matrix(ncol=(length(gofvector)), nrow=nrun)
# matrix for the flow duration curve
MFDC <- matrix(ncol=nrun, nrow=length(sort.length))

for (i in 1:nrun) {  

  # selection of results of the model run i
  q <- qmod[,(i+1)]
  ql <- length(q)
  
  qsimdate <- as.Date(as.character(qmod$date), "%Y-%m-%j")
  qsimdatenew<-format(qsimdate, "%m/%d/%Y")

  # modelled discharge data formatted as time series
  xts.sim<-as.xts(q, order.by =as.Date(as.character(qsimdatenew), "%m/%d/%Y"))
  calsim<-xts.sim[index(calobsdischarge)]

  # FDC segment calculation
  sort.sim <- sort(as.numeric(calsim[,1]))
  
  MFDC[,i] <- sort.sim
  
  sim005<- as.numeric(sort.sim[round(length(sort.sim)*0.95,0):round(length(sort.sim)*1.0,0)])
  sim0520<- as.numeric(sort.sim[round(length(sort.sim)*0.80,0):round(length(sort.sim)*0.95,0)])
  sim2070<- as.numeric(sort.sim[round(length(sort.sim)*0.30,0):round(length(sort.sim)*0.80,0)])
  sim7095<- as.numeric(sort.sim[round(length(sort.sim)*0.05,0):round(length(sort.sim)*0.30,0)])
  sim95100<- as.numeric(sort.sim[1:round(length(sort.sim)*0.05,0)]) 
  
  # calculation of a long list of multiple performance measures
  # first simulated than observed time series
  # digits define the number of decimal places
    calfit <- gof(as.numeric(calsim[,1]),as.numeric(calobsdischarge), digits=6)
  
  # KGE beta
  # please consider that KGE beta and PBIAS have a correlation of one meaning that these performance measures represents exact the same hydrological behaviour, but with different values
  beta <- (mean(as.numeric(calsim[,1]))/mean(as.numeric(calobsdischarge[,1])))
      
  vhigh <- gof(sim005, obs005, digits=12)
  high <- gof(sim0520, obs0520, digits=12)
  mid <- gof(sim2070, obs2070, digits=12)
  low <- gof(sim7095, obs7095, digits=12)
  vlow <- gof(sim95100, obs95100, digits=12)
  
  # summary matrix
  MGOF[i,1]<- i
# each number defines one performance measure: 
# 9 = NSE
# 6 = PBIAS
# 19 = KGE
# 7 = RSR
# 8= KGE_alpha
# 16 = KGE_r
  MGOF[i,c(2:7)]<-calfit[c(9,6,20 ,7, 8, 17)]
  MGOF[i,c(8:13)]<-c(beta, vhigh[7], high[7], mid[7], low[7], vlow[7]) 
  
  
  # plot
  setwd(paste("C:\\KlimaW\\Lippe\\Simulations\\plot", sep=""))  
   newnumber<-sprintf("%05d", i)
  
  #PDF drucken
  pdf(file = paste("qfull", newnumber, ".pdf", sep=""), onefile=T, paper="special", width=15, height=8, pointsize=20)
  
   plot(index(calobsdischarge), calobsdischarge, ylab="Discharge [m?/s]", 
        xlab="", type = "l", col="white")
   lines(index(calobsdischarge), calobsdischarge, col="blue", lwd=1)
   lines(index(calsim), calsim[,1], col="red", lwd=1)
  
  legend("topright", lty=c(1,1,1), col=c("blue", "red"), 
         legend=c("Q measured", "Q modelled"))
    title(main=paste("modelrun: ", newnumber, sep=""), outer=FALSE)
  
   dev.off()
  
    fdcmatrix<-matrix(nrow=length(calobsdischarge), ncol=2)
    fdcmatrix[,1]<-as.numeric(calobsdischarge)
    fdcmatrix[,2]<-as.numeric(calsim[,1])
  
  #PDF drucken  
   pdf(file = paste("fdc", newnumber, ".pdf", sep=""), onefile=T, 
       paper="special", width=15, height=8, pointsize=20)
  
    fdc.matrix(fdcmatrix,  leg.txt=c("obs","sim"), 
               ylab="Q m3/s", xlab="% Discharge equalled or exceeded")
  
   dev.off()
  
}  

colnames(MGOF) <- gofvector

#set working directory
setwd(swat_wd)

write.table(file=paste("GOFrun", ".txt",sep=""), MGOF, col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)

summary(MGOF)

MGOF <- as.data.frame(MGOF)

NSE_quant <- quantile(MGOF$NSE, probs=seq(0,1,0.02))

NSE_best20 <- MGOF[MGOF$NSE>NSE_quant[length(NSE_quant)-1],]
# look at best model run NSE

NSE_best <- MGOF[MGOF$NSE>0.65,]
NSE_vlow_best <- NSE_best[NSE_best$very_low<4,]

MOF_best <- MGOF[MGOF$KGE>0.6 & MGOF$NSE>0.5 & MGOF$PBIAS>-10 & MGOF$PBIAS<10 & MGOF$mid<0.5 & MGOF$very_low<5,]


# Euclidean distance
meanFDC <- apply(MGOF[,9:13], 1, mean)
MGOF$meanFDC <- meanFDC

ED <- sqrt(meanFDC^2+MGOF$KGE^2)
MGOF$ED <- ED

ed_quant <- quantile(ED, probs=seq(0,1,0.01))
ED_best <- MGOF[ED<ed_quant[2],]

# combination of NSE and ED
NSE_ED_best <- MGOF[MGOF$NSE>0.5 & MGOF$ED<1.8,]


## validation ####


# selection of the modeling period
valobsdischarge<-xts.obs['2016-11-01/2021-10-31']
# three year warm up
# five year calibration
# five year validation

# discharge data in the order of their magnitude
sort.obs <- sort(as.numeric(valobsdischarge))
sort.length<-1:length(sort.obs)

# separation of the ordered discharge data into five segments
# very high flows
obs005<- as.numeric(sort.obs[round(length(sort.obs)*0.95,0):round(length(sort.obs)*1.0,0)])
# high flows
obs0520<- as.numeric(sort.obs[round(length(sort.obs)*0.80,0):round(length(sort.obs)*0.95,0)])
obs2070<- as.numeric(sort.obs[round(length(sort.obs)*0.30,0):round(length(sort.obs)*0.80,0)])
obs7095<- as.numeric(sort.obs[round(length(sort.obs)*0.05,0):round(length(sort.obs)*0.30,0)])
# very low flows
obs95100<- as.numeric(sort.obs[1:round(length(sort.obs)*0.05,0)]) 

###

# matrix to save results of performance criteria
MGOF_val<-matrix(ncol=(length(gofvector)), nrow=nrun)
# matrix for the flow duration curve
MFDC_val <- matrix(ncol=nrun, nrow=length(sort.length))

for (i in 1:nrun) {  
  
  # selection of results of the model run i
  q<- qmod[,(i+1)]
  ql <- length(q)
  
  qsimdate <- as.Date(as.character(qmod$date), "%Y-%m-%j")
  qsimdatenew<-format(qsimdate, "%m/%d/%Y")
  
  # modelled discharge data formatted as time series
  xts.sim<-as.xts(q, order.by =as.Date(as.character(qsimdatenew), "%m/%d/%Y"))
  valsim<-xts.sim[index(valobsdischarge)]
  
  # FDC segment calculation
  sort.sim <- sort(as.numeric(valsim[,1]))
  
  MFDC_val[,i] <- sort.sim
  
  sim005<- as.numeric(sort.sim[round(length(sort.sim)*0.95,0):round(length(sort.sim)*1.0,0)])
  sim0520<- as.numeric(sort.sim[round(length(sort.sim)*0.80,0):round(length(sort.sim)*0.95,0)])
  sim2070<- as.numeric(sort.sim[round(length(sort.sim)*0.30,0):round(length(sort.sim)*0.80,0)])
  sim7095<- as.numeric(sort.sim[round(length(sort.sim)*0.05,0):round(length(sort.sim)*0.30,0)])
  sim95100<- as.numeric(sort.sim[1:round(length(sort.sim)*0.05,0)]) 
  
  # calculation of a long list of multiple performance measures
  # first simulated than observed time series
  # digits define the number of decimal places
  calfit<-gof(as.numeric(valsim[,1]),as.numeric(valobsdischarge), digits=6)
  
  # KGE beta
  # please consider that KGE beta and PBIAS have a correlation of one meaning that these performance measures represents exact the same hydrological behaviour, but with different values
  beta <- (mean(as.numeric(valsim[,1]))/mean(as.numeric(valobsdischarge[,1])))
  
  vhigh <- gof(sim005, obs005, digits=12)
  high <- gof(sim0520, obs0520, digits=12)
  mid <- gof(sim2070, obs2070, digits=12)
  low <- gof(sim7095, obs7095, digits=12)
  vlow <- gof(sim95100, obs95100, digits=12)
  
  # summary matrix
  MGOF_val[i,1]<- i
  # each number defines one performance measure: 
  # 9 = NSE
  # 6 = PBIAS
  # 20 = KGE
  # 7 = RSR
  # 8= KGE_alpha
  # 17 = KGE_r
  MGOF_val[i,c(2:7)]<-calfit[c(9,6, 20,7, 8, 17)]
  MGOF_val[i,c(8:13)]<-c(beta, vhigh[7], high[7], mid[7], low[7], vlow[7]) 
  
  
  # plot
  setwd(paste("C:\\KlimaW\\Lippe\\Simulations\\plot_val", sep=""))  
  newnumber<-sprintf("%05d", i)
  
  #PDF drucken
  pdf(file = paste("qfull", newnumber, ".pdf", sep=""), onefile=T, paper="special", width=15, height=8, pointsize=20)
  
  plot(index(valobsdischarge), valobsdischarge, ylab="Discharge [m?/s]", xlab="", type = "l", col="white")
  lines(index(valobsdischarge), valobsdischarge, col="blue", lwd=1)
  lines(index(valsim), valsim[,1], col="red", lwd=1)
  
  legend("topright", lty=c(1,1,1), col=c("blue", "red"), legend=c("Q measured", "Q modelled"))
  title(main=paste("modelrun: ", newnumber, sep=""), outer=FALSE)
  
  dev.off()
  
  fdcmatrix<-matrix(nrow=length(calobsdischarge), ncol=2)
  fdcmatrix[,1]<-as.numeric(calobsdischarge)
  fdcmatrix[,2]<-as.numeric(calsim[,1])
  
  #PDF drucken  
  pdf(file = paste("fdc", newnumber, ".pdf", sep=""), onefile=T, paper="special", width=15, height=8, pointsize=20)
  
  fdc.matrix(fdcmatrix,  leg.txt=c("obs","sim"), 
             ylab="Q m3/s", xlab="% Discharge equalled or exceeded")
  
  dev.off()
  
}  

colnames(MGOF_val) <- gofvector

#set working directory
setwd(swat_wd)

write.table(file=paste("GOFrun_val", ".txt",sep=""), MGOF_val, col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)

summary(MGOF_val)

MGOF_val <- as.data.frame(MGOF_val)

NSE_best20_val <- MGOF_val[MGOF$NSE>NSE_quant[length(NSE_quant)-1],]

# look at best model run NSE
NSE_best_val <- MGOF_val[MGOF$NSE>0.5,]
MOF_best_val <- MGOF_val[MGOF$KGE>0.7 & MGOF$NSE>0.5 & MGOF$PBIAS >-10 & MGOF$PBIAS<10 & MGOF$mid<0.5 & MGOF$very_low<5,]



# Euclidean distance
meanFDC_val <- apply(MGOF_val[,9:13], 1, mean)
MGOF_val$meanFDC <- meanFDC_val

ED_val <- sqrt(meanFDC_val^2+MGOF_val$KGE^2)
MGOF_val$ED <- ED_val

ED_best_val <- MGOF_val[ED<ed_quant[2],]

# combination of NSE and ED
NSE_ED_best_val <- MGOF_val[MGOF$NSE>0.5 & MGOF$ED<1.8,]
NSE_ED_best_val_cal <- MGOF_val[MGOF_val$NSE>0.5 & MGOF_val$ED<2,]

