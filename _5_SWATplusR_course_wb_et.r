# r script for calibration course
# cau kiel
# 12/2019
# Bj?rn Guse, support by Christoph Sch?rz (BOKU Vienna)

# taken from https://chrisschuerz.github.io/SWATplusR/

# If you do not have the package devtools installed
install.packages("devtools")
#install.package("assertthat", "memoise", "desc", "rprojroot", "sessioninfo", "tidyr")

devtools::install_github("chrisschuerz/SWATplusR")


# SWAT library for R
library(SWATplusR)
library(tidyr)
library(xts)

# discharge data

# set working directory
path_plus <- "C:\\stkm\\SWAT+_simulations\\TxtInOut"
setwd(path_plus)

save_plus <- "C:\\stkm\\SWAT+_simulations\\Results"

# model run
# only one run
setwd(save_plus)

# Comparison of Penman-Monteith and Hargreaves ####

# one simulation round as a test

wb_sim <- run_swatplus(project_path = path_plus,
                       output = list(q_total = define_output(file = "channel_sd",
                                                             variable = "flo_out",
                                                             unit = 1),
                                     precip = define_output(file = "basin_wb",
                                                            variable = "precip",
                                                            unit = 1),
                                     q_sur  = define_output(file = "basin_wb",
                                                            variable = "surq_gen",
                                                            unit = 1),
                                     q_lat  = define_output(file = "basin_wb",
                                                            variable = "latq",
                                                            unit = 1),
                                     eta    = define_output(file = "basin_wb",
                                                            variable = "et",
                                                            unit = 1)),
                       revision=60.5)


et_PM <- wb_sim$eta
q_PM <- wb_sim$q_total


# changes pet from 1 to 2 in codes.bsn in the path_plus folder

wb_sim <- run_swatplus(project_path = path_plus,
                       output = list(q_total = define_output(file = "channel_sd",
                                                             variable = "flo_out",
                                                             unit = 1),
                                     precip = define_output(file = "basin_wb",
                                                            variable = "precip",
                                                            unit = 1),
                                     q_sur  = define_output(file = "basin_wb",
                                                            variable = "surq_gen",
                                                            unit = 1),
                                     q_lat  = define_output(file = "basin_wb",
                                                            variable = "latq",
                                                            unit = 1),
                                     eta    = define_output(file = "basin_wb",
                                                            variable = "et",
                                                            unit = 1)),
                       revision=60.5)

et_HAR <- wb_sim$eta
q_HAR <- wb_sim$q_total

# plot of the first five model runs to see the variability in ET due to different parameter sets

datevector<-seq(as.Date("2010/01/01"), as.Date("2017/12/31"), by="days")
xts.et_PM<-as.xts(et_PM, order.by =as.Date(as.character(datevector), "%Y-%m-%d"))
xts.et_HAR<-as.xts(et_HAR, order.by =as.Date(as.character(datevector), "%Y-%m-%d"))

xts_et_all <- cbind(xts.et_PM, xts.et_HAR)

xts.q_PM<-as.xts(q_PM, order.by =as.Date(as.character(datevector), "%Y-%m-%d"))
xts.q_HAR<-as.xts(q_HAR, order.by =as.Date(as.character(datevector), "%Y-%m-%d"))

xts_q_all <- cbind(xts.q_PM, xts.q_HAR)

pdf(file = paste("et_PM_HAR.pdf", sep=""), 
    onefile=T, paper="special", width=20, height=20, pointsize=20)

par(mfrow=c(2,1))

plot(xts_et_all, type="l", col=c("black", "blue"), ylab="Daily ET")
par(new=T)
plot(log10(xts_q_all), type="l", col=c("black", "blue"), ylab="Daily Q in log10", log='y')

dev.off()

# parameter ranges ####

# see cal_parms.cal
par_table <- matrix(nrow=9, ncol=2)
par_table[1,] <- c(-15, -5) # cn2
par_table[2,] <- c(0.25, 0.75) # cn3_swf
par_table[3,] <- c(0.1, 1.0) # esco
par_table[4,] <- c(0.5, 1) # perco
par_table[5,] <- c(-0.01, 0.2) # awc
par_table[6,] <- c(-2.5, 2.5) # snofall_tmp
par_table[7,] <- c(-2.5, 2.5) # snomelt_tmp
par_table[8,] <- c(0.2, 2) # surlag
par_table[9,] <- c(0, 1) # alpha
colnames(par_table) <- c("rangeMin", "rangeMax")
par_table <- as.data.frame(par_table)

# LHS parameter sampling
library(FME)

minvalue<-as.numeric(par_table$rangeMin)
maxvalue<-as.numeric(par_table$rangeMax)

namevalue <- c("cn2.hru|change = abschg", "cn3_swf.hru|change = absval",
               "esco.hru|change = absval", "perco.hru|change = absval" ,
               "awc.sol|change = abschg" ,"snofall_tmp.hru|change = absval",
               "snomelt_tmp.hru|change = absval", "surlag.bsn|change = absval",
               "alpha.aqu|change = absval" )

parRange <- data.frame(min = minvalue, max = maxvalue)
rownames(parRange) <- namevalue

# number of model runs
nrun <- 5

# the number (here 4) defines the number of parameter sets and thus the number of model simulations
paras<-Latinhyper(parRange, nrun)

summary(paras)

# preparation of parameter sets for calculation
par_set_lh <- tibble(paras[,1], paras[,2], paras[,3], paras[,4], paras[,5], paras[,6], paras[,7], paras[,8], paras[,9])
colnames(par_set_lh) <- namevalue

setwd(save_plus)

write.table(file=paste("swatplus_par_wb_221202.txt",sep=""), par_set_lh, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)


# water balance and processes

WB_SIM <- run_swatplus(project_path = path_plus,
                       output = list(q_total = define_output(file = "channel_sd",
                                                             variable = "flo_out",
                                                             unit = 1),
                                     precip = define_output(file = "basin_wb",
                                                            variable = "precip",
                                                            unit = 1),
                                     q_sur  = define_output(file = "basin_wb",
                                                            variable = "surq_gen",
                                                            unit = 1),
                                     q_lat  = define_output(file = "basin_wb",
                                                            variable = "latq",
                                                            unit = 1),
                                     percol = define_output(file = "basin_wb",
                                                            variable = "perc",
                                                            unit = 1),
                                     q_tile = define_output(file = "basin_wb",
                                                            variable = "qtile",
                                                            unit = 1),
                                     eta    = define_output(file = "basin_wb",
                                                            variable = "et",
                                                            unit = 1)),
                       parameter = par_set_lh,
                       revision=60.5,
                       n_thread = 8)


param <- WB_SIM$parameter$values

# evapotranspiration ####

Et_all <- WB_SIM$simulation$eta


write.table(file=paste("swatplus_sim_221202_eta", ".txt",sep=""), Et_all, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)



# plot of the first five model runs to see the variability in ET due to different parameter sets
param <- read.table(paste(getwd(),"\\swatplus_par_221202.txt", sep=""), sep=";", header=T)
parnames <- names(param)

et_mean <- apply(Et_all[,2:(nrun+1)], 2, mean)

Et_all2 <- as.data.frame(Et_all)

pdf(file = paste("etall.pdf", sep=""), 
    onefile=T, paper="special", width=20, height=20, pointsize=20)

plot(Et_all2[1:365,2], type="l", ylim=c(0,8), col="black", ylab="Daily ET", xlab= "")
par(new=T)
plot(Et_all2[1:365,3], type="l", ylim=c(0,8), col="blue", axes=F, ylab= "", xlab = "")
par(new=T)
plot(Et_all2[1:365,4], type="l", ylim=c(0,8), col="green", axes=F, ylab= "", xlab = "")
par(new=T)
plot(Et_all2[1:365,5], type="l", ylim=c(0,8), col="red", axes=F, ylab= "", xlab = "")
par(new=T)
plot(Et_all2[1:365,6], type="l", ylim=c(0,8), col="violet", axes=F, ylab= "", xlab = "")
par(new=T)

dev.off()

parnames <- names(param)


#PDF drucken  


pdf(file = paste("par_vs_eta.pdf", sep=""), 
    onefile=T, paper="special", width=20, height=20, pointsize=20)

par(mfrow=c(3,3))
par(mar=c(5,5,1,1), cex.lab=1.6, cex.axis=1.2)

  plot(param[1:nrun,1], as.numeric(et_mean), pch=16, xlab=parnames[1], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,2], et_mean, pch=16, xlab=parnames[2], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,3], et_mean, pch=16, xlab=parnames[3], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,4], et_mean, pch=16, xlab=parnames[4], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,5], et_mean, pch=16, xlab=parnames[5], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,6], et_mean, pch=16, xlab=parnames[6], ylab="Mean ET")
  par(new=F)
  plot(param[1:nrun,7], et_mean, pch=16, xlab=parnames[7], ylab="Mean ET")

dev.off()

