# Load the SWAT-RCH-Output Data into R   by PDW

path_plus <- "C:\\KlimaW\\Lippe\\Simulations"
setwd(path_plus)

### The following variables need to be adjusted ###
  #Simulation Data
  outletchannel<-58                       # Number of last channel, above the river gauge Soltfeld
  SWATtxtinout<-'C:\\KlimaW\\Lippe\\Simulations\\TxtInOut'    # The folder, where the channel_sdmorph_day.txt file is located.
  starting_date_sim<-"2011-01-01"       # Starting date of the printed simulation results.
                                        # If you used a warm-up time, this is different from the starting date of the simulation.
  #Observed Data
  Observed_runoff_path<- "C:\\KlimaW\\Lippe\\SWAT input Lippe\\Gauges\\Schermbeck 1_R.txt"  # Name and path of the file with the observed data
  starting_date_obs<-"2010-01-01"   # Starting date of the observations
  
### End of adjustments ###
                                                 
### Read the observed data ###
  obs<-read.table(Observed_runoff_path)
  obs[,4]<-as.Date((1:length(obs$V2))-1,origin=starting_date_obs)    # add date
  names(obs)<-c("index","origdate","OUTFLOW","DATE")      # rename variables
  head(obs)
  tail(obs)
  

### Read the simulation data ###  
  reach <- read.table(paste(SWATtxtinout,'/channel_sdmorph_day.txt',sep=''), header=F, skip=3)
  subrch<-subset(reach, reach$V5==outletchannel)     # restrict model output to river gauge Soltfeld
  names(subrch)[10]<-'flo_out'
  subrch[26]<-as.Date((1:length(subrch$V1))-1,origin=starting_date_sim)     # add date
  names(subrch)[26]<-c("DATE")  # name variable
     subch1<-subrch



### Plot the hydrographs ###  
  plot(subrch$DATE,subrch$flo_out, type='l',col='blue')
  lines(obs$DATE,obs$OUTFLOW, type='l',col='red') 
 
 
### Restrict to the period of observation (calibration period)###
  begindate<- "2011-01-01"
  enddate<-   "2016-12-31"
  subdat_sim<-subset(subrch, DATE>=begindate & DATE<=enddate)
  subdat_obs<-subset(obs, DATE>=begindate & DATE<=enddate)
  plot(subdat_sim$DATE,subdat_sim$flo_out, type='l', col='blue')        # Plot the hydrographs
  lines(subdat_obs$DATE,subdat_obs$OUTFLOW, col='red')

### Restrict to only one year
  begindate<- "2011-01-01"
  enddate<-   "2011-12-31"
  subdat_sim<-subset(subrch, DATE>=begindate & DATE<=enddate)
  subdat_obs<-subset(obs, DATE>=begindate & DATE<=enddate)
  plot(subdat_sim$DATE,subdat_sim$flo_out, type='l', col='blue')        # Plot the hydrographs
  lines(subdat_obs$DATE,subdat_obs$OUTFLOW, col='red')



    #Goodness_of_fit_function definition
    goodfit<-function(mes,mod) {

    goodfit_tab<-data.frame('MAE'=NA,'MaxAE'=NA,'RMSE'=NA,'NSE'=NA)  #Result table of goodness-of-fit measures
    dif<-mes-mod
    MAE  <- mean(abs(dif))                             # mean absolute error
    RMSE <- sqrt(mean(dif*dif))            # root mean square error
    NSE  <- 1 - (  (sum((mes-mod)^2)) /    # Nash-Sutcliffe
               (sum((mes-mean(mes))^2))   )
    MaxAE<-max(abs(dif))
    PBIAS<-100*(sum(dif)/sum(mes))

    goodfit_tab<-round(c('MAE'=MAE,'MaxAE'=MaxAE,'RMSE'=RMSE,'NSE'=NSE,'PBIAS'=PBIAS),3)    # Round and write goodness-of-fit measures into result table
    return(goodfit_tab)
    }

   # Application of the goodfit function to our data
   goodfit(subdat_obs$OUTFLOW,subdat_sim$flo_out)
   
   ## A Sann