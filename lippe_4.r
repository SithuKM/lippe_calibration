#set working directory
swat_wd <- "C:\\KlimaW\\Lippe\\Simulations\\Results"

setwd(swat_wd)

# table with model results for each model simulation
gof <- read.table(paste(getwd(),"/GOFrun.txt", sep=""), 
                  header=TRUE, sep=";")
gofnames <- names(gof)

gofnames[6:9] <- c("KGE_alpha", "KGE_r", "KGE_beta", "Very_high")
gofnames[13] <- c("very_low")
names(gof) <- gofnames
gofnames <- names(gof)


# table with model with parameter settings for each model simulation
par <- read.table(paste("C:\\KlimaW\\Lippe\\Simulations\\Results\\swatplus_par_230203.txt", sep=""), sep=";", header=T)
parnames <- names(par)

parcol <- ncol(par)
gofcol <- ncol(gof)

#bestruns <- c(678,750,980)
#bestruns <- c(20, 154, 164, 251, 486)
bestruns <- c(67,139)

bestcol <- c("red", "green", "blue", "orange", "violet")
#rainbow(3)

# joint plot for all

# maxline defines the maximum values of the objective functions
# if you change the functions or their order, you have to update this vector
maxline <- c(0,1,0,1,0, rep(1,3), rep(0,5))

#PDF drucken  
pdf(file = paste("par_vs_gof_230203.pdf", sep=""), 
    onefile=T, paper="special", width=25, height=17, pointsize=20)

par(mfrow=c(3,4))
par(mar=c(5,5,1,1), cex.lab=1.6, cex.axis=1.2)


for (i1 in 1:parcol) {
  parnow <- parnames[i1]
  
    for (i2 in 2:gofcol) {
    gofnow <- gofnames[i2]

    parmin <- min(par[,i1])
    parmax <- max(par[,i1])
    gofmin <- min(gof[,i2], maxline[i2])
    gofmax <- max(gof[,i2], maxline[i2])
    
    plot(par[,i1], gof[,i2], pch=16, xlab=parnow, ylab=gofnow, 
         cex=1.2, 
         xlim=c(parmin, parmax), ylim=c(gofmin, gofmax))
    # horizontale line shows the optimum value of the objective function
    abline(h=maxline[i2])
    par(new=T)
    
    plot(par[bestruns,i1], gof[bestruns,i2], 
         pch=16, xlab="", ylab="", 
         cex=1.6, col=bestcol,
         xlim=c(parmin, parmax), ylim=c(gofmin, gofmax))
    
   par(new=F)

  }
  
}

dev.off()


