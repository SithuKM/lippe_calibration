# r script for calibration course
# cau kiel
# 12/2019
# Bj?rn Guse, support by Christoph Sch?rz (BOKU Vienna)

# taken from https://chrisschuerz.github.io/SWATplusR/

# If you do not have the package devtools installed
install.packages("devtools")
install.packages("assertthat", "memoise", "desc", "rprojroot", "sessioninfo", "tidyr")

devtools::install_github("chrisschuerz/SWATplusR")


# SWAT library for R
library(SWATplusR)
library(tidyr)
library(dplyr)

# set working directory
path_plus <- "C:\\KlimaW\\Lippe\\Simulations\\TxtInOut"
setwd(path_plus)

save_plus <- "C:\\KlimaW\\Lippe\\Simulations\\Results"

# model run
# only one run
q_sim_plus_1 <- run_swatplus(project_path = path_plus,
                           output = define_output(file = "channel_sd",
                                                  variable = "flo_out",
                                                  unit = 58),
                           revision=60.5)

# restrict to modelling period: 
q_sim_plus_1 <- filter(q_sim_plus_1, date >= "2011-11-01") # start in hydrologic year

plot(q_sim_plus_1$date,q_sim_plus_1$flo_out, type='l',col='blue')

write.table(file=paste("first_swatR_simulation", ".txt",sep=""), q_sim_plus_1, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)


setwd(save_plus)


wb_sim <- run_swatplus(project_path = path_plus,
                       output = list(precip = define_output(file = "basin_wb",
                                                            variable = "precip",
                                                            unit = 1),
                                     q_sur  = define_output(file = "basin_wb",
                                                            variable = "surq_gen",
                                                            unit = 1),
                                     q_lat  = define_output(file = "basin_wb",
                                                            variable = "latq",
                                                            unit = 1),
                                     eta    = define_output(file = "basin_wb",
                                                            variable = "et",
                                                            unit = 1)))


write.table(file=paste("swatplus_wb_230203", ".txt",sep=""), wb_sim, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)



# parameter ranges ####

# see cal_parms.cal
par_table <- matrix(nrow=12, ncol=2)
par_table[1,] <- c(-15, 2) # cn2
par_table[2,] <- c(0, 1) # cn3_swf
par_table[3,] <- c(0, 1) # esco
par_table[4,] <- c(0, 1) # perco
par_table[5,] <- c(-0.01, 0.2) # awc
par_table[6,] <- c(0.1, 12) # surlag
par_table[7,] <- c(0, 1) # alpha
par_table[8,] <- c(-2, 2) # snofall_tmp
par_table[9,] <- c(-2, 2) # snomelt_tmp
par_table[10,] <- c(0.5, 100) # lat_ttime
par_table[11,] <- c(0, 10) # canmx
par_table[12,] <- c(-50, 200) # k
par_table[13,] <- c(0,50) #flo_min

colnames(par_table) <- c("rangeMin", "rangeMax")
par_table <- as.data.frame(par_table)


minvalue<-as.numeric(par_table$rangeMin)
maxvalue<-as.numeric(par_table$rangeMax)


namevalue <- c("cn2.hru|change = abschg", "cn3_swf.hru|change = absval",
               "esco.hru|change = absval", "perco.hru|change = absval" ,
               "awc.sol|change = abschg" , "surlag.bsn|change = absval",
               "alpha.aqu|change = absval", "snofall_tmp.hru|change = absval",
               "snomelt_tmp.hru|change = absval", "lat_ttime.hru|change = absval",
               "canmx.hru|change = absval", "k.sol|change = pctchg",
               "flo_min.aqu|change = absval")

parRange <- data.frame(min = minvalue, max = maxvalue)
rownames(parRange) <- namevalue

# LHS parameter sampling
library(FME)

# the number (here 4) defines the number of parameter sets and thus the number of model simulations
paras<-Latinhyper(parRange, 10)

summary(paras)

# preparation of parameter sets for calculation

par_set_lh <- tibble(paras[,1], paras[,2], paras[,3], paras[,4], paras[,5], paras[,6], paras[,7], paras[,8], paras[,9], paras[,10], paras[,11], paras[,12], paras[,13])
colnames(par_set_lh) <- namevalue

write.table(file=paste("swatplus_par_230203(10).txt",sep=""), par_set_lh, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)


# model runs with sampled parameters ####
# the number of model simulations is defined by the number of parameters in par_set_lh
q_sim_plus <- run_swatplus(project_path = save_plus,
                           output = define_output(file = "channel_sd",
                                                  variable = "flo_out",
                                                  unit = 58),
                           parameter = par_set_lh,
                           revision = 60.5,
                           keep_folder = TRUE,  #activation for the final run to keep the parameter values
                           n_thread = 12)
# modelled discharge
qmod <- q_sim_plus$simulation$flo_out

# check if simulated discharge are really different
head(qmod)

# saving model simulations and associated parameters
# if you run multi-simulations (over long time) 
# please evaluate also the following lines to store the results in the case of a computer new start
write.table(file=paste("C:\\KlimaW\\Lippe\\Simulations\\Results\\swatplus_sim_230203(10)", ".txt",sep=""), qmod, 
            col.names=TRUE, row.names=FALSE, sep=";", quote=FALSE)


# for the final calculation with the best parameter sets:
# 1. Parameter ranges are set identical to the best parameter values, i.e. par_table[9,] <- c(0.233, 0.233) # alpha
# 2. paras<-Latinhyper(parRange, 1)
# 3. activate keep_folder = TRUE
# 4. the subfolder .model_run\thread_1 is kept
# 5. final parameter set is stored in calibration.cal

# if you want to reload modelled simulations,
# please activate the next line
# select the folder
qmod <-as.data.frame(read.table("C:\\KlimaW\\Lippe\\Simulations\\Results\\swatplus_sim_230203.txt", sep=";", header=T))

# modelled discharge plot
plot(qmod$run_1, type="l", col='black')


#Observed Data
Observed_runoff_path<- 'C:\\KlimaW\\Lippe\\SWAT input Lippe\\Gauges\\Schermbeck 1_R.txt'  # Name and path of the file with the observed data
starting_date_obs<-"2010-01-01"   

### Read the observed data ###   
obs<-read.table(Observed_runoff_path)
obs[4]<-as.Date((1:length(obs$V1))-1,origin=starting_date_obs)    # add date
names(obs)<-c("COUNT","V2","OUTFLOW","DATE")  
library(ggplot2)
qplot(data=obs, x=DATE, y=OUTFLOW, geom="line")
### Plot the hydrographs ###  
plot(as.Date(qmod$date),qmod$run_1, type='l',col='black')
lines(obs$DATE,obs$OUTFLOW, type='l',col='blue') 

plot(obs$DATE,obs$OUTFLOW, type='l',col='black')
lines(as.Date(qmod$date),qmod$run_1, type='l',col='blue') 

### Restrict to the  calibration period ###
begindate<- "2007-01-01"
enddate<-   "2018-01-01"
subdat_sim<-subset(qmod, as.Date(date)>=begindate & as.Date(date)<=enddate)
subdat_obs<-subset(obs, DATE>=begindate & DATE<=enddate)
subdat_date <-as.Date(subdat_sim$date)

plot(as.Date(subdat_sim$date),subdat_sim$run_1, type='l', col='blue', ylim=c(0,10))        # Plot the hydrographs
lines(subdat_obs$DATE,subdat_obs$OUTFLOW, col='red')
par(new=T)
plot(as.Date(subdat_sim$date),subdat_sim$run_2, type='l', col='green', axes=F, ylim=c(0,10))

